﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hola_Mundo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = "¡Hola Mundo!\n¡Mi nombre es Megumin!";
            this.BackColor = Color.DarkGoldenrod;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = "¡Explosion!\n Digo, Adiós Mundo.";
            this.BackColor = Color.Red;
        }
    }
}
